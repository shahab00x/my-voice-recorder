package com.example.bazibazi.myvoicerecorder;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by admin on 12/10/2016.
 */

public class myListViewAdapter extends ArrayAdapter<RecordingFile> {

    public myListViewAdapter(Context context, ArrayList<RecordingFile> rfvs) {
        super(context, 0, rfvs);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            LayoutInflater inflater = LayoutInflater.from(getContext());
            listItemView = inflater.inflate(R.layout.recordedfile, parent, false);
        }

        RecordingFile rfv = getItem(position);

        TextView fileName = (TextView) listItemView.findViewById(R.id.recordedFileName);
        TextView modifiedDate = (TextView) listItemView.findViewById(R.id.recordedFileModifiedDate);
        ImageView playButton = (ImageView) listItemView.findViewById(R.id.recordedPlayButton);

        final String recording_directory = rfv.getFileLocation();
        final String recording_file_name = rfv.getFileName();


        fileName.setText(recording_file_name);
        modifiedDate.setText(rfv.getModifiedDate());

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyAudioRecord.play(recording_directory, recording_file_name);
                ImageView iv = (ImageView) view.findViewById(R.id.recordedPlayButton);
                iv.setImageResource(R.drawable.play_red);
            }
        });

//        playButton.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                ImageView iv = (ImageView) view.findViewById(R.id.recordedPlayButton);
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    iv.setImageResource(R.drawable.play_white);
//                }
//                return true;
//            }
//        });

        return listItemView;
    }
}
