/*
 * The application needs to have the permission to write to external storage
 * if the output file is written to the external storage, and also the
 * permission to record audio. These permissions must be set in the
 * application's AndroidManifest.xml file, with something like:
 *
 * <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 * <uses-permission android:name="android.permission.RECORD_AUDIO" />
 *
 */
package com.example.bazibazi.myvoicerecorder;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.os.Bundle;
import android.os.Environment;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.util.Log;
import android.media.MediaRecorder;
import android.media.MediaPlayer;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.os.Vibrator;

public class AudioRecorder extends Activity {
    private static final String LOG_TAG = "AudioRecordTest";
    public static String mFileName = "test.mp3";
    private static String recordingDirectory;
    private MediaRecorder mRecorder = null;

    private boolean saveRecordings = false;

    private MediaPlayer mPlayer = null;
    private boolean recording = false;
    private static Button my_button;
    private long last_recording_number = 0;
    private SharedPreferences settings;

    public AudioRecorder(Button b, String file_name, String recording_directory){
        mFileName = file_name;
        my_button = b;
        recordingDirectory = recording_directory;
        File dir = new File(recording_directory);
        if (!dir.exists())
            dir.mkdir();
        settings = b.getContext().getSharedPreferences("my_voice_recorder_settings", 0);
        if (!settings.contains("last_file_number")) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putLong("last_file_number", 0);
            editor.commit();
        }else{
            last_recording_number = settings.getLong("last_file_number", 0);
        }
    }

    @Override
    protected void onDestroy() {


        super.onDestroy();
    }

    public void saveRecordings(boolean value){
        saveRecordings = value;
    }
    
    public void toggleRecordAndPlayback() {
        Toast my_toast = Toast.makeText(my_button.getContext(), "Recording", Toast.LENGTH_SHORT);
        if (recording == false) {
            try {
                stopPlaying();
            } catch (Exception e) {
            }
            startRecording();
            recording = true;
            my_toast.cancel();
            my_toast.setText("Recording");
            my_toast.show();
            my_button.setBackgroundColor(Color.BLACK);
            my_button.setTextColor(Color.RED);
            my_button.setTextSize(40);

            try {
                Thread.sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Vibrator v = (Vibrator) my_button.getContext().getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
            my_button.setText("Recording");
//            my_button.setWidth(Math.round(my_button.getWidth()*1.2f));
//            my_button.setHeight( Math.round(my_button.getHeight()*1.2f));
            my_button.getLayoutParams().width *= 1.2f;
            my_button.getLayoutParams().height *= 1.2f;

        } else if (recording == true) {
            try {
                stopRecording();
            } catch (Exception e) {
            }

            startPlaying();
            recording = false;
            my_toast.cancel();
            my_toast.setText("Playing");
            my_toast.show();
            my_button.setBackgroundColor(Color.BLACK);
            my_button.setTextColor(Color.WHITE);
            my_button.setTextSize(25);
            my_button.setText("Record");
//            my_button.setWidth(Math.round(my_button.getWidth()/1.2f));
//            my_button.setHeight( Math.round(my_button.getHeight()/1.2f));
            my_button.getLayoutParams().width /= 1.2f;
            my_button.getLayoutParams().height /= 1.2f;
        }


    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private boolean isPreprated = false;
    private void prepare(){
        if (! isPreprated) {
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setOutputFile(mFileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            isPreprated = true;
        }
    }
    private void startRecording() {
        prepare();

        try {
            mRecorder.prepare();

        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }


        mRecorder.start();
    }

    public void copy_file(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    private void stopRecording() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

        if (saveRecordings) {
            File src = new File(mFileName);
            File dst = new File(recordingDirectory + "/shahab_says_" + Long.toString(last_recording_number) + ".mp3");
            try {
                copy_file(src, dst);
                last_recording_number += 1;

                SharedPreferences.Editor editor = settings.edit();
                editor.putLong("last_file_number", last_recording_number);
                editor.commit();
            } catch (Exception e) {
            }
        }
        isPreprated = false;
        prepare(); // Prepare for the next recording
    }
}