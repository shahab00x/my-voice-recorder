package com.example.bazibazi.myvoicerecorder;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * TODO: document your custom view class.
 */
public class RecordingFileView extends LinearLayout {
    TextView file_name;
    private TextView playTextView;
    private String recording_directory;

    public RecordingFileView(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.sample_recording_file_view, this);

        init(context);
    }

    public RecordingFileView(Context context, AttributeSet attr){
        super(context, attr);
        init(context);
    }

    public RecordingFileView(Context context, AttributeSet attr, int defStyle){
        super(context, attr, defStyle);
        init(context);
    }
    private void init(Context context) {
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
        file_name = (TextView) findViewById(R.id.textView_file_name);
//        TextView playTextView = new TextView(getContext());
//        playTextView.setText("Play Me");
//        playTextView.setTextColor(Color.WHITE);
//        playTextView.setClickable(true);

        playTextView = (TextView) findViewById(R.id.playRecordingButton);

        recording_directory = Environment.getExternalStorageDirectory().getAbsolutePath() + "/shahab_recordings";

//        playTextView.setOnClickListener(playButtonClickListener);

//        this.addView(playTextView);
    }

    public void initialize(){

        View.OnClickListener myClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyAudioRecord.play(recording_directory, (String) file_name.getText() );
                playTextView.setTextColor(Color.RED);
                file_name.setTextColor(Color.GRAY);
            }
        };

        playTextView.setOnClickListener(myClickListener);
    }

    public void setPlayButtonOnClickListener(OnClickListener myClickListener ){
        playTextView.setOnClickListener( myClickListener);
    }

    public void setFileNameText(String text) {

        file_name.setText(text);
    }

    public void buttonPlayClicked(View view){
        Toast my_toast = Toast.makeText(getContext(), "hello", Toast.LENGTH_SHORT);
        my_toast.show();
    }
}