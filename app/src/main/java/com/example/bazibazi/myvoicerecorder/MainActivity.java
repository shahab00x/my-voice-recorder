package com.example.bazibazi.myvoicerecorder;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;

import com.example.bazibazi.myvoicerecorder.MyAudioRecord;

public class MainActivity extends AppCompatActivity {

    MyAudioRecord audio_recorder;
    PowerManager.WakeLock wl;

    private SensorManager sensorManager;
    private Sensor lightSensor;
    private float lightAmount;

    private enum RecordingMedium {PROXIMITY, BUTTON, VOLUME_UP, BLUETOOTH_BUTTON, UNSET}

    ;
    private RecordingMedium recording_medium = RecordingMedium.UNSET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        SensorEventListener sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (!audio_recorder.isRecording && sensorEvent.values[0] <= 5) {
                    audio_recorder.toggleRecordAndPlayback();
                    recording_medium = RecordingMedium.PROXIMITY;
                }
//                    audio_recorder.startRecording();
                else if (audio_recorder.isRecording && sensorEvent.values[0] > 5 && recording_medium == RecordingMedium.PROXIMITY)
//                    audio_recorder.stopRecording();
                    audio_recorder.toggleRecordAndPlayback();
                Log.v("Proximity :::", sensorEvent.values[0] + "");
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
                Log.v("Proximity = ", "" + i);
            }
        };

        sensorManager.registerListener(sensorEventListener, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);


        final Button record_button = (Button) findViewById(R.id.buttonRecord);

        String mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
        String recording_directory = mFileName + "/shahab_recordings";
        mFileName += "/audio_record_test";
        audio_recorder = new MyAudioRecord(record_button, mFileName, recording_directory);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WAKE_LOCK}, 0);
        }


        record_button.setOnTouchListener(new View.OnTouchListener() {
                                             @Override
                                             public boolean onTouch(View view, MotionEvent motionEvent) {
                                                 if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                                                     audio_recorder.toggleRecordAndPlayback();


                                                 }
                                                 if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                                                     audio_recorder.toggleRecordAndPlayback();

                                                 }
                                                 return false;
                                             }
                                         }
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wl.release();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP && event.getRepeatCount() == 0 && event.ACTION_DOWN == MotionEvent.ACTION_DOWN) {
            audio_recorder.toggleRecordAndPlayback();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP && event.ACTION_UP == MotionEvent.ACTION_UP && event.getRepeatCount() == 0) {
            audio_recorder.toggleRecordAndPlayback();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN && event.getRepeatCount() == 0 && event.ACTION_DOWN == MotionEvent.ACTION_DOWN) {
//                toggleRecordAndPlayback();
            audio_recorder.playLastOne();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN && event.getRepeatCount() > 0) {
//                toggleRecordAndPlayback();

            return true;
        }

//        keyCode == KeyEvent.KEYCODE_BUTTON_Y ||
//                keyCode == KeyEvent.KEYCODE_DPAD_DOWN   ||
//                keyCode == KeyEvent.KEYCODE_DPAD_LEFT   ||
//                keyCode == KeyEvent.KEYCODE_DPAD_RIGHT  ||
//                keyCode == KeyEvent.KEYCODE_DPAD_UP
        // Use bluetooth gamepad to record
        else if ((keyCode == KeyEvent.KEYCODE_BUTTON_X ||
                keyCode == KeyEvent.KEYCODE_BUTTON_A ||
                keyCode == KeyEvent.KEYCODE_BUTTON_B) && event.getRepeatCount() == 0 && event.ACTION_DOWN == MotionEvent.ACTION_DOWN) {
            audio_recorder.toggleRecordAndPlayback();
        }


        return true;
    }


    public void RecordingsListButton(View v) {
        Intent intent = new Intent(this, RecordingsListActivity.class);
        startActivity(intent);
    }

    boolean saveRecordings = false;

    public void saveRecordings(View v) {
        Button button_save_recording = (Button) findViewById(R.id.buttonSaveRecordings);
        if (saveRecordings == false) {
            saveRecordings = true;
            button_save_recording.setTextColor(Color.GREEN);
            button_save_recording.setText("Save Recordings: Yes");
        } else {
            saveRecordings = false;
            button_save_recording.setTextColor(Color.RED);
            button_save_recording.setText("Save Recordings: No");
        }
        audio_recorder.saveRecordings(saveRecordings);
    }
}


