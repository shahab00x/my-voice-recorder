package com.example.bazibazi.myvoicerecorder;

import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Attr;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class RecordingsListActivity extends AppCompatActivity {

    private MyAudioRecord audio_recorder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordings_list);

        final String recording_directory = Environment.getExternalStorageDirectory().getAbsolutePath() + "/shahab_recordings";

//        LinearLayout lv = (LinearLayout) findViewById(R.id.recording_scroller_list);
        File dir = new File(recording_directory);
        File file_names[] = dir.listFiles();

        ArrayList<RecordingFile> rf = new ArrayList<RecordingFile>();

        for (int i= 0;i < file_names.length; i++) {
            rf.add( new RecordingFile(file_names[i], recording_directory));
        }

        myListViewAdapter itemsAdapter = new myListViewAdapter(this, rf);

        ListView lv = (ListView) findViewById(R.id.list);

        lv.setAdapter( itemsAdapter);
    }
}


