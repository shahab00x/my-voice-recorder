package com.example.bazibazi.myvoicerecorder;

import java.io.File;
import java.util.Date;

/**
 * Created by admin on 1/4/2017.
 */

public class RecordingFile {

    private File mFile;
    private String mFileName;
    private String mFileLocation;
    private String mModifiedDate;

    public RecordingFile(File file, String fileLocation){
        setData(file, fileLocation);
    }

    private void setData(File file, String fileLocation){
        mFile = file;
        mFileLocation = fileLocation;
        mFileName = file.getAbsoluteFile().getName();

        Date d = new Date( file.lastModified());
        mModifiedDate = d.toString();
    }

    public File getFile(){ return mFile;}

    public String getFileName(){
        return mFileName;
    }

    public String getFileLocation(){
        return mFileLocation;
    }
    public String getModifiedDate(){return mModifiedDate;}
}
